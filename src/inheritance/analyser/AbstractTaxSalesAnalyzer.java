package inheritance.analyser;


public class AbstractTaxSalesAnalyzer {

    protected final Double VAT = 0.2;
    protected final SalesRecord[] records;

    public AbstractTaxSalesAnalyzer(SalesRecord[] records) {
        this.records = records;
    }

    public Double getTotalSales() {
        double salesTaxIncluded = 0.0;
        for (SalesRecord product : records) {
            int salesWithoutTax = product.getItemsSold() * product.getProductPrice();
            if (product.hasReducedRate()) {
                double reducedVAT = 1 + reducedGetVAT();
                salesTaxIncluded += salesWithoutTax / reducedVAT;
            } else {
                double VAT = 1 + getVAT();
                salesTaxIncluded += salesWithoutTax / VAT;
            }
        }
        return salesTaxIncluded;
    }

    public Double getTotalSalesByProductId(String id) {
        for (SalesRecord product : records) {
            if (product.getProductId().equals(id)) {
                System.out.println("nothing");
            }
        }
        return null;
    }

    public String getIdOfMostPopularItem() {
        throw new RuntimeException("not implemented yet");
    }

    public String getIdOfItemWithLargestTotalSales() {
        throw new RuntimeException("not implemented yet");
    }

    protected Double getVAT() {
        return VAT;
    }

    protected Double reducedGetVAT() {
        return getVAT();
    }
}
