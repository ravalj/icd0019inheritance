package inheritance.analyser;

public class DifferentiatedTaxSalesAnalyser extends AbstractTaxSalesAnalyzer {

    public DifferentiatedTaxSalesAnalyser(SalesRecord[] records) {
        super(records);
    }

    @Override
    protected Double reducedGetVAT() {
        return 0.1;
    }

    @Override
    protected Double getVAT() {
        return 0.2;
    }
}
