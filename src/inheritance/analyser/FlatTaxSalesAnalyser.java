package inheritance.analyser;

public class FlatTaxSalesAnalyser extends AbstractTaxSalesAnalyzer{

    public FlatTaxSalesAnalyser(SalesRecord[] records) {
        super(records);
    }

    @Override
    protected Double getVAT() {
        return 0.2;
    }
}
