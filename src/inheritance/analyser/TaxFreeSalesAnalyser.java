package inheritance.analyser;

public class TaxFreeSalesAnalyser extends AbstractTaxSalesAnalyzer {

    public TaxFreeSalesAnalyser(SalesRecord[] records) {
        super(records);
    }

    @Override
    protected Double getVAT() {
        return 0.0;
    }
}
